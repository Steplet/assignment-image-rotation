#include<error_flags.h>
#include<image.h>
#include<stdbool.h>
#include<stdio.h>


enum error_flags only_read_file(const char* path_file, struct image* source_input_image);
enum error_flags only_write_file(const char* path_file, struct image* source_output_image);
enum error_flags close_file(FILE* name_file);
