#ifndef MY_ASSIGNMENT_IMAGE_ROTATION_ERROR_FLAGS_H
#define MY_ASSIGNMENT_IMAGE_ROTATION_ERROR_FLAGS_H

enum error_flags {
        SUCESS,
        FILE_READ_ERROR ,
        FILE_WRITE_ERROR ,
        FILE_CLOSE_ERROR ,
        BMP_SEEK_ERROR,
        HEADER_ERROR,
        BMP_READ_ERROR,
        BMP_WRITE_ERROR, 
};

void print_error(enum error_flags e);
#endif //MY_ASSIGNMENT_IMAGE_ROTATION_ERROR_FLAGS_H
