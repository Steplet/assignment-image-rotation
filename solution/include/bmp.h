
#include<error_flags.h>
#include<image.h>
#include<stdbool.h>
#include<stdint.h>
#include<stdio.h>




enum error_flags from_bmp(FILE* input_file, struct image* image);
enum error_flags to_bmp(FILE* output_file, struct image* image);
