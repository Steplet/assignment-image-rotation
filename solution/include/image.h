#ifndef MY_ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define MY_ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include<malloc.h>
#include<stdint.h>

#pragma pack(push, 1)
struct pixel  {
	uint8_t b, g, r;
};
#pragma pack(pop)


struct image{
	uint64_t width, height;
	struct pixel* data;
};

uint32_t get_height(const struct image* image);
uint32_t get_width(const struct image* image);
struct pixel* get_pixels(const struct image* image);
uint8_t get_padding(const uint64_t width);

struct image create_image(const uint64_t width, const uint64_t height);
void delete_image(const struct image image);

#endif //MY_ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
