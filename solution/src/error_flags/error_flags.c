#include<../include/error_flags.h>
#include<stdio.h>



void print_error( enum error_flags e) {
    switch (e )
    {
    case SUCESS:
        fprintf(stderr, "%s", "Success.");
        fprintf(stderr, "%s", "\n");
        break;
    case FILE_READ_ERROR:
        fprintf(stderr, "%s", "Can`t read a file.");
        fprintf(stderr, "%s", "\n");
        break;
    case FILE_WRITE_ERROR:
        fprintf(stderr, "%s", "Can`t write a file.");
        fprintf(stderr, "%s", "\n");
        break;
    case BMP_WRITE_ERROR:
        fprintf(stderr, "%s", "Can`t write bmp file.");
        fprintf(stderr, "%s", "\n");
        break;
    case HEADER_ERROR:
        fprintf(stderr, "%s", "Error bmp header.");
        fprintf(stderr, "%s", "\n");
        break;
    case BMP_SEEK_ERROR:
        fprintf(stderr, "%s", "Error bmp seek .");
        fprintf(stderr, "%s", "\n");
        break;
    case FILE_CLOSE_ERROR:
        fprintf(stderr, "%s", "Can`t close a file.");
        fprintf(stderr, "%s", "\n");
        break;
    case BMP_READ_ERROR:
        fprintf(stderr, "%s", "Can`t read bmp file.");
        fprintf(stderr, "%s", "\n");
        break;
    
    default:
        fprintf(stderr, "%s", "Something wrong");
        fprintf(stderr, "%s", "\n");
        break;
    }
    

}
