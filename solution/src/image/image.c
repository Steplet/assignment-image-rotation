#include<../include/image.h>
#include <stdlib.h>


uint32_t get_height(const struct image*  image)
{

	return (int32_t)image->height;
}

uint32_t get_width(const struct image*  image)
{
	return (int32_t)image->width;
}

struct pixel* get_pixels(const struct image* image)
{
	return image->data;
}

struct image create_image(const uint64_t width, const uint64_t height)
{
    void* data = malloc(sizeof(struct pixel) * height * width);
    if (data == NULL){
        fprintf(stdout, "Memory error");
        exit(1);
    }
	struct image image = { .height = height, .width = width, 
							.data = data };
	return image;
}

void delete_image(const struct image image)
{
	free(image.data);
}

uint8_t get_padding(const uint64_t width){
	return  width % 4;
}
