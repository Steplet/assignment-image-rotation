#include<../include/bmp.h>

#include<../include/image.h>


#define BMP_TYPE  19778
#define ZERO_VALUE 0
#define BI_SIZE 40
#define BIT_COUNT 24
#define PELS_PER_METER 2834
#define PLANES 1

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};



#pragma pack(pop)

struct bmp_header create_bmp_header(struct image* image)
{
    uint32_t size_bmp_struct = sizeof(struct bmp_header);

    uint32_t size_pixel_struct = sizeof(struct pixel) *  image->width *  image->height ;
    struct bmp_header bmp_header =
    {
        

        .bfType = BMP_TYPE,
            .bfileSize =  size_bmp_struct + size_pixel_struct,
            .bfReserved = ZERO_VALUE,
            .bOffBits = size_bmp_struct,
            .biSize = BI_SIZE,
            .biWidth = get_width(image),
            .biHeight = get_height(image),
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = ZERO_VALUE,
            .biSizeImage = size_pixel_struct,
            .biXPelsPerMeter = PELS_PER_METER,
            .biYPelsPerMeter = PELS_PER_METER,
            .biClrUsed = ZERO_VALUE,
            .biClrImportant = ZERO_VALUE

                };
	return bmp_header;
}

enum error_flags from_bmp(FILE* input_file, struct image* image)
{
    
    struct bmp_header head = {0};

    if(!(fread(&head, sizeof(struct bmp_header), 1, input_file))) 
    {
        return HEADER_ERROR;
    }

     *image = create_image(head.biWidth,head.biHeight);

     if(fseek(input_file, sizeof(struct bmp_header), 0)!= 0){ 
        return BMP_SEEK_ERROR;
     }

    uint8_t padding = get_padding(image->width);
    
    for (size_t i = 0; i < image->height; i++)
    {
        if(!fread(image->data + image->width * i, sizeof(struct pixel) * image->width, 1, input_file)){
            return BMP_READ_ERROR;
        }
        
        if(fseek(input_file, padding, 1)!= 0){
            return BMP_SEEK_ERROR;
        }
    }
    
	return SUCESS;
}

enum error_flags to_bmp(FILE* output_file, struct image* image)
{
    struct bmp_header new_head = create_bmp_header(image);
    if(!fwrite(&new_head, sizeof(struct bmp_header), 1, output_file)){
        return BMP_WRITE_ERROR;
    }

    uint8_t padding = get_padding(image->width);
    uint8_t paddings[3] = {0};
    for (size_t i = 0; i < image->height; i++)
    {
        if(!fwrite(image->data + image->width * i, sizeof(struct pixel) * image->width, 1, output_file)){
            return BMP_WRITE_ERROR;
        }
        if(!fwrite(paddings, padding, 1, output_file)){
            return BMP_WRITE_ERROR;
        }
    }
    
	return SUCESS;
}
