#include<../include/IO.h>
#include<../include/image.h>
#include<../include/transformations_image.h>
#include <stdio.h>


int main( int argc, char** argv ) {

    char* input_file_path = argv[1];
    char* output_file_path = argv[2];

    if (argc != 3)
    {
        fprintf(stdout, "Wrong numbers of args");
        return 0;
    }
    

    struct image normal_input_image = {0};
    
    enum error_flags state = only_read_file(input_file_path, &normal_input_image);
    print_error(state);
    if(state != SUCESS){
        delete_image(normal_input_image);
       
        return 0;

    }


    struct image transform_output_image = rotate(normal_input_image);

    state = only_write_file(output_file_path, &transform_output_image);
    print_error(state);
    if( state != SUCESS){
        delete_image(normal_input_image);
        delete_image(transform_output_image);
        return 0;
    }

    delete_image(normal_input_image);
    delete_image(transform_output_image);

    return 0;
}
