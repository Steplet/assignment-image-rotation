#include<../include/transformations_image.h>

struct image rotate(struct image image)
{
    uint64_t new_height = image.width;
    uint64_t new_width = image.height;

    struct image output_image = create_image(new_width, new_height);

    for (size_t i = 0; i < new_height; i++)
    {
        for (size_t j = 0; j < new_width; j++)
        {
            output_image.data[i * new_width + new_width - 1 - j] = image.data[j * image.width + i] ;
        }
        
    }
    return output_image;
    
}
