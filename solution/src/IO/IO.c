#include<../include/IO.h>
#include<../include/bmp.h>

enum error_flags only_read_file(const char* path_file, struct image* source_input_image)
{
	FILE* file_name;
	file_name = fopen(path_file, "rb"); 
	if (file_name == NULL)
	{
		return FILE_READ_ERROR;
	}
	else
	{
		enum error_flags state = from_bmp(file_name, source_input_image);
		if(state != SUCESS){
			return state;
		}
		return close_file(file_name);
	}
	
}

enum error_flags only_write_file(const char* path_file, struct image* source_output_image)
{
	FILE* file_name;
	file_name = fopen(path_file, "wb");
	if (file_name == NULL)
	{
		return FILE_WRITE_ERROR;
	}
	else
	{
		enum error_flags state =  to_bmp(file_name,source_output_image);
		if (state != SUCESS)
		{
			return state;
		}
		
		
		return close_file(file_name);
	}
	
}

enum error_flags close_file(FILE* name_file)
{
	
	if (fclose(name_file) == -1)
	{
		return FILE_CLOSE_ERROR;
	}
	else
	{
		return SUCESS;
	}
	
}
